# freedesktop.org Helm infra deployment

This repository holds the deployments of our runners and our k3s infrastructure.


## Fedora CoreOS deployments

We rely on iPXE for the CoreOS deployment. However, there is a small catch in which iPXE currently
doesn't handle properly TLS certificates from Let's Encrypt.

So instead of relying on a remote iPXE file, we simply provide the iPXE script as the Equinix Metal
user data, and not use that user data as the ignition data as expected by the ignition packet
provider.

#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

# Copyright © 2021-2023 Benjamin Tissoires

from typing import Dict, Optional, Any, List
from pathlib import Path

import base64
import click
import equinix_metal
import ipaddress
import json
import logging
import os
import paramiko  # for handling ssh
import random
import re
import scp
import shlex
import shutil
import subprocess
import sys
import tempfile
import time
import uuid
import yaml

logger = logging.getLogger("fdo-infra")


def xdg_config_home() -> Path:
    return Path(os.environ.get("XDG_CONFIG_HOME", Path.home() / ".config"))


def xdg_token(name: str, project: str = "fdo-infra") -> Path:
    return xdg_config_home() / project / name


def xdg_metal_id(project: str = "fdo-infra") -> Path:
    return xdg_token("metal.id", project)


def xdg_metal_token(project: str = "fdo-infra") -> Path:
    return xdg_token("metal.token", project)


def die(message: str):
    click.echo(message)
    raise SystemExit(1)


# based on https://github.com/hackersandslackers/paramiko-tutorial/blob/master/paramiko_tutorial/client.py
# MIT Licensed
class RemoteClient(object):
    """Client to interact with a remote host via SSH & SCP."""

    def __init__(
        self,
        name: str,
        config: Dict,
    ):
        device = _get_metal_device(name, config)
        assert device is not None
        ssh_params = device.customdata["ssh"]
        self.host = public_ipv4(name, config)
        self.user = ssh_params["user"]
        self.port = ssh_params["port"]
        self._client = self.new_connection()
        self._scp = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect()

    def new_connection(self):
        """Open connection to remote host."""
        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(
                self.host,
                username=self.user,
                port=self.port,
                timeout=5000,
            )
            return client
        except paramiko.auth_handler.AuthenticationException as e:
            logger.error(
                f"Authentication failed: did you remember to create an SSH key? {e}"
            )
            raise e

    def new_scp(self) -> scp.SCPClient:
        conn = self.new_connection()
        return scp.SCPClient(conn.get_transport())

    @property
    def scp(self):
        if self._scp is None:
            self._scp = self.new_scp()

        return self._scp

    def disconnect(self):
        """Close SSH & SCP connection."""
        self._client.close()
        if self._scp is not None:
            self._scp.close()

    def bulk_upload(self, files: List[str], remote_path: str):
        """
        Upload multiple files to a remote directory.
        :param files: List of local files to be uploaded.
        :type files: List[str]
        """
        try:
            logger.info("About to send files via scp...")
            self.scp.put(files, remote_path=remote_path)
            logger.info(
                f"Finished uploading {len(files)} files to {remote_path} on {self.host}"
            )
        except scp.SCPException as e:
            raise e

    def download_file(self, file: str):
        """Download file from remote host."""
        self.scp.get(file)

    def execute_commands(self, commands: List[str]):
        """
        Execute multiple commands in succession.
        :param commands: List of unix commands as strings.
        :type commands: List[str]
        """
        compact = len(commands) > 10
        if compact:
            command = ";".join(commands)
            commands = [
                f"bash -c '{command}'",
            ]
        for cmd in commands:
            stdin, stdout, stderr = self._client.exec_command(cmd)
            stdout.channel.recv_exit_status()
            response = stdout.readlines()
            cmd = f"{cmd[:50]}..." if compact else cmd
            for line in response:
                logger.info(f"INPUT: {cmd} | OUTPUT: {line.rstrip()}")
            errs = stderr.readlines()
            for line in errs:
                logger.warning(f"INPUT: {cmd} | OUTPUT: {line.rstrip()}")

    def execute_command(self, command: str):
        """
        Execute one command.
        :param command: the unix command as a string.
        :type command: str
        """
        stdin, stdout, stderr = self._client.exec_command(command)
        status = stdout.channel.recv_exit_status()
        return status, stdout, stderr


def load_config(project_id: str, project_token: str) -> Dict:
    if not project_id:
        die(
            "project-id missing, either set METAL_PROJECT_ID environment variable, either use --project-id"
        )

    script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    # if not project_token:
    #     logger.error(
    #         "project-token missing, either set METAL_AUTH_TOKEN environment variable, either use --project-token"
    #     )
    #     sys.exit(1)

    with open(script_dir / "config.yaml") as f:
        config = yaml.safe_load(f)

    metal_conf = equinix_metal.Configuration()
    if project_token is not None:
        metal_conf.api_key["x_auth_token"] = project_token

    config["manager"] = equinix_metal.ApiClient(metal_conf)
    config["project-id"] = project_id
    config["project-token"] = project_token

    return config


def get_config_for_device(name: str, config: Dict) -> Dict:
    re_name = re.compile(r"(?P<type>.*)-(?P<id>[0-9]+)")
    m = re_name.match(name)

    if m is None:
        die(f"name {name} is not following the pattern 'type-id'")

    assert m is not None

    _type = m.group("type")
    _id = m.group("id")

    config["type"] = _type
    config["id"] = _id

    if _type not in config:
        die(f"type of server name '{name}' is unknown")

    return config[_type]


def _get_metal_device(name: str, config: Dict, optional: bool = False) -> Optional[Any]:
    if "devices" not in config:
        devices_api = equinix_metal.DevicesApi(config["manager"])

        answer = devices_api.find_project_devices(id=config["project-id"], per_page=50)

        for i in answer:
            if i[0] == "devices":
                config["devices"] = i[1]

    our_devices = [d for d in config["devices"] if d.hostname == name]

    if not len(our_devices):
        if not optional:
            die(f"device '{name}' doesn't exist")
        else:
            return None
    elif len(our_devices) > 1:
        die(f"more than one device with '{name}' exist, can not continue")

    return our_devices[0]


def _get_ip(name, config, version, public):
    device = _get_metal_device(name, config)

    if device is None:
        return None

    ip_assignments = [
        ip
        for ip in device.ip_addresses
        if ip.address_family == version and ip.public == public
    ]

    for ip in ip_assignments:
        return ip.address

    return None


def public_ipv4(name, config):
    return _get_ip(name, config, 4, public=True)


def public_ipv6(name, config):
    return _get_ip(name, config, 6, public=True)


def private_ipv4(name, config):
    return _get_ip(name, config, 4, public=False)


def layer2_ipv4(name, config):
    device = _get_metal_device(name, config)

    ip_mask = device.customdata["vlans"][0]["ip"]
    return ip_mask[: ip_mask.index("/")]


def layer2_ipv6(name, config):
    device = _get_metal_device(name, config)

    ip_mask = device.customdata["vlans"][0]["ipv6"]
    return ip_mask[: ip_mask.index("/")]


@click.group()
@click.option(
    "--project-id",
    help="the project UUID on packet",
    default=os.getenv("METAL_PROJECT_ID"),
)
@click.option(
    "--project-id-file",
    help=f"File containing the project UUID on Equinix Metal. Defaults to {xdg_metal_id()}",
    type=click.Path(exists=True, dir_okay=False),
)
@click.option(
    "--project-token",
    help="the project token on packet",
    default=os.getenv("METAL_AUTH_TOKEN"),
)
@click.option(
    "--project-token-file",
    help=f"File containing the project token on Equinix Metal. Defaults to {xdg_metal_token()}",
    type=click.Path(exists=True, dir_okay=False),
)
@click.option("--verbose", "-v", is_flag=True, help="Enable debug logging")
@click.pass_context
def fdo_infra(
    ctx, project_id, project_id_file, project_token, project_token_file, verbose
):
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)
    logger.addHandler(logging.StreamHandler(sys.stderr))

    # ensure that ctx.obj exists and is a dict (in case `fdo_infra()` is called
    # by means other than the `if __name__` block below)
    ctx.ensure_object(dict)

    def get_token(token: str, token_file: click.Path, name: str):
        if token is not None:
            return token

        if token_file is not None and token_file.exists():
            return open(token_file).read().rstrip() or die(f"Empty {name} file")

        die(f"{name} is not set, please set it at {token_file}")

    ctx.obj["project_id"] = get_token(
        project_id, project_id_file or xdg_metal_id(), "Project ID"
    )
    ctx.obj["project_token"] = get_token(
        project_token, project_token_file or xdg_metal_token(), "Metal Auth Token"
    )


@fdo_infra.command(name="ssh", help="ssh to an already deployed machine")
@click.argument("name")
@click.pass_context
def fdo_ssh(ctx, name: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])
    logger.debug(config)
    logger.debug("-------------")

    device = _get_metal_device(name, config)
    logger.debug(device)
    logger.debug("-------------")

    assert device is not None

    if "ssh" not in device.customdata:
        device.customdata["ssh"] = {"user": "root", "port": 22}

    sys.stdout.write(
        f'ssh -p {device.customdata["ssh"]["port"]} {device.customdata["ssh"]["user"]}@{public_ipv4(name, config)}\n'
    )


@fdo_infra.command(name="console", help="ssh to an already deployed machine")
@click.argument("name")
@click.pass_context
def fdo_console(ctx, name: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])
    logger.debug(config)
    logger.debug("-------------")

    device = _get_metal_device(name, config)
    logger.debug(device)
    logger.debug("-------------")

    assert device is not None

    if "ssh" not in device.customdata:
        device.customdata["ssh"] = {"user": "root", "port": 22}

    sys.stdout.write(
        f"ssh -o HostkeyAlgorithms=ssh-rsa -o PubkeyAcceptedKeyTypes=ssh-rsa {device.id}@sos.{device.facility.code}.platformequinix.com\n"
    )


def wait_for_device_active(client, did):
    wait_timeout = time.time() + time.time() + 60 * 5
    while wait_timeout > time.time():
        device = equinix_metal.DevicesApi(client).find_device_by_id(did)
        if device.state == "active":
            return device
        logger.info(
            f"Waiting for device {did} to become active, device state is {device.state}"
        )
        time.sleep(5)
    raise Exception("Timed out waiting for device to become active")


def find_vlan(config, vlan_number):
    vlan_api = equinix_metal.VLANsApi(config["manager"])
    vlans_resp = vlan_api.find_virtual_networks(config["project-id"])

    for type, obj in vlans_resp:
        if type == "virtual_networks" and obj is not None:
            for v in obj:
                if v.vxlan == vlan_number:
                    return v

    return None


def setup_vlan(config, device, vxlan):
    assert device.network_ports is not None
    bond = None
    for port in device.network_ports:
        if port.name == "bond0":
            bond = port

    assert bond is not None

    metal_vlan = find_vlan(config, vxlan)

    assert metal_vlan is not None

    # we need to wait for the device to become active before enabling vlan
    wait_for_device_active(config["manager"], device.id)

    logger.info("Device is active")

    if metal_vlan.href in [v.href for v in bond.virtual_networks]:
        logger.info(f"VLAN {metal_vlan.vxlan} already assigned to device {device.id}")

    else:
        pai = equinix_metal.PortAssignInput(vnid=metal_vlan.id)
        ports_api = equinix_metal.PortsApi(config["manager"])

        ports_api.assign_port(bond.id, pai)

        logger.info(
            f"Successfully assigned VLAN {metal_vlan.vxlan} to device {device.id}"
        )


def which_gomplate() -> str:
    # check if gomplate is installed first
    gomplate = shutil.which("gomplate")

    if gomplate is None:
        die(
            "Please install `gomplate` first by following instructions at https://docs.gomplate.ca/installing/"
        )

    assert gomplate is not None

    return gomplate


def do_gomplate(file: str, env: Dict = {}) -> str:
    gomplate = which_gomplate()

    with subprocess.Popen(
        [
            gomplate,
            "--template",
            "utils=cloud-init/utils.gotmpl",
            "--datasource",
            "config=config.yaml",
            "--file",
            file,
        ],
        stdout=subprocess.PIPE,
        cwd=os.path.dirname(os.path.realpath(__file__)),
        env=env,
    ) as proc:
        assert proc.stdout is not None
        return proc.stdout.read().rstrip().decode("utf8")

    assert True


@fdo_infra.command(name="create", help="Deploy a new machine")
@click.argument("name")
@click.pass_context
def fdo_create(ctx, name: str):
    which_gomplate()

    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])
    logger.debug(config)
    logger.debug("-------------")

    device = _get_metal_device(name, config, optional=True)
    logger.debug(device)
    logger.debug("-------------")

    if device is not None:
        die(
            f"Device {name} already exists, please reinstall it with the `reinstall` command. Aborting."
        )

    device_config = get_config_for_device(name, config)

    if "vlan" in device_config:
        v = device_config["vlan"]
        vlan = {
            "vlan": f"{v['id']}",
            "ip": f"{v['ipv4']['prefix']}{config['id']}/{v['ipv4']['netmask']}",
            "ipv6": f"{v['ipv6']['prefix']}{config['id']}/{v['ipv6']['netmask']}",
        }

    userdata = do_gomplate(
        "cloud-init/cloud-init.gotmpl", env={"METAL_DEVICE_NAME": name}
    )

    customdata = {"environment": {}, "ssh": device_config["ssh"], "vlans": [vlan]}

    cdm = equinix_metal.DeviceCreateInMetroInput(
        operating_system=config["operating_system"],
        plan=device_config["metal"]["plan"],
        hostname=name,
        metro=device_config["metal"]["metro"],
        billing_cycle="hourly",
        tags=device_config["metal"]["tags"].split(","),
        customdata=customdata,
        userdata=userdata,
    )  # type: ignore

    cdr = equinix_metal.CreateDeviceRequest(actual_instance=cdm)

    devices_api = equinix_metal.DevicesApi(config["manager"])

    new_device_resp = devices_api.create_device(config["project-id"], cdr)

    assert new_device_resp is not None

    logger.info(f"Device {new_device_resp.id} successfully created")

    if "vlan" in device_config:
        setup_vlan(config, new_device_resp, device_config["vlan"]["id"])
    else:
        wait_for_device_active(config["manager"], new_device_resp.id)
        logger.info("Device is active")


@fdo_infra.command(
    name="reinstall", help="Reinstall an existing machine with the current data"
)
@click.argument("name")
@click.pass_context
def fdo_reinstall(ctx, name: str):
    # check if gomplate is installed first
    which_gomplate()

    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])
    logger.debug(config)
    logger.debug("-------------")

    device = _get_metal_device(name, config, optional=False)
    logger.debug(device)
    logger.debug("-------------")

    assert device is not None

    device_config = get_config_for_device(name, config)

    v = device_config["vlan"]
    vlan = {
        "vlan": f"{v['id']}",
        "ip": f"{v['ipv4']['prefix']}{config['id']}/{v['ipv4']['netmask']}",
        "ipv6": f"{v['ipv6']['prefix']}{config['id']}/{v['ipv6']['netmask']}",
    }

    userdata = do_gomplate(
        "cloud-init/cloud-init.gotmpl", env={"METAL_DEVICE_NAME": name}
    )

    customdata = {"environment": {}, "ssh": device_config["ssh"], "vlans": [vlan]}

    logger.warning(f"I will reinstall completely {name} in 5 secs. Hit Ctrl-C to abort")

    time.sleep(5)

    dui = equinix_metal.DeviceUpdateInput(
        customdata=customdata,
        userdata=userdata,
        tags=device_config["metal"]["tags"].split(","),
    )  # type: ignore

    devices_api = equinix_metal.DevicesApi(config["manager"])

    updated_device_resp = devices_api.update_device(device.id, dui)

    assert updated_device_resp is not None

    logger.info(f"Device {device.id} successfully reconfigured")

    dai = equinix_metal.DeviceActionInput(
        type="reinstall",
        operating_system=config["operating_system"],
    )  # type: ignore

    devices_api.perform_action(device.id, dai)

    logger.info(f"Device {device.id} reinstalling...")

    if "vlan" in device_config:
        setup_vlan(config, device, device_config["vlan"]["id"])
    else:
        wait_for_device_active(config["manager"], device.id)
        logger.info("Device is active")


def setup_server(name: str, config: Dict, ssh_con: RemoteClient, agent: bool = False):
    public_ip = public_ipv4(name, config)
    private_ip = private_ipv4(name, config)
    _public_ipv6 = public_ipv6(name, config)

    commands = [
        # replace the placeholders values
        f"sed -i 's/SERVER_PUBLIC_IP/{public_ip}/' /root/k3s-config-common.yaml",
        f"sed -i 's/SERVER_PUBLIC_IPV6/{_public_ipv6}/' /root/k3s-config-common.yaml",
        f"sed -i 's/SERVER_PRIVATE_IP/{private_ip}/' /root/metallb-bgp.yaml",
        "sudo mkdir -p /etc/rancher/k3s/config.yaml.d/",
        "sudo cp /root/k3s-config-common.yaml /etc/rancher/k3s/config.yaml",
    ]

    if not agent:
        commands.append(
            "sudo cp /root/k3s-config-server.yaml /etc/rancher/k3s/config.yaml.d/server.yaml"
        )

    ssh_con.execute_commands(commands)


def enable_bgp(name, config, ssh_con):
    device = _get_metal_device(name, config)
    devices_api = equinix_metal.DevicesApi(config["manager"])
    private_ip = private_ipv4(name, config)

    bgp = do_gomplate(
        "cloud-init/k3s/files.d/root/metallb-bgp.yaml", env={"METAL_DEVICE_NAME": name}
    )

    bgp = bgp.replace("SERVER_PRIVATE_IP", private_ip)

    kubectl_apply_str(ssh_con, bgp)

    resp = devices_api.get_bgp_neighbor_data(device.id)

    if len(resp.bgp_neighbors) == 2:
        logger.info(f"{name} already has BGP enabled")
        return

    missing = ["ipv4", "ipv6"]

    for neighbor in resp.bgp_neighbors:
        family = f"ipv{ neighbor.address_family}"
        if family in missing:
            missing.remove(family)

    for family in missing:
        bsi = equinix_metal.BGPSessionInput(address_family=family)

        logger.info(f"enable BGP {family} for device {name}")
        devices_api.create_bgp_session(device.id, bsi)
        logger.info("Done")


@fdo_infra.command(name="cluster-init")
@click.argument("name")
@click.pass_context
def cluster_init(ctx, name: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logger.debug(config)

    logger.info(f"connecting to {name}")
    with RemoteClient(name, config) as ssh_con:
        setup_server(name, config, ssh_con, False)

        # install k3s
        command = f"""curl -sfL https://get.k3s.io | \
                      INSTALL_K3S_CHANNEL={config['k3s']['channel']} sh -s - server --cluster-init"""
        ssh_con.execute_commands([command])

        logger.info("k3s deployed, waiting for it to be ready")
        command = "kubectl wait node --all --for condition=ready --timeout=600s"
        ssh_con.execute_commands([command])

        logger.info("k3s deployed, now installing basic requirements")
        kustomization = """
resources:
- https://raw.githubusercontent.com/metallb/metallb/v0.13.11/config/manifests/metallb-frr.yaml
images:
- name: quay.io/metallb/controller
  newName: quay.io/bentiss/metallb/controller
  newTag: v0.13.11-bentiss
- name: quay.io/metallb/speaker
  newName: quay.io/bentiss/metallb/speaker
  newTag: v0.13.11-bentiss
"""
        kubectl_apply_kustomize_str(ssh_con, kustomization)

        deploy_kilo(ssh_con, name)

        command = "timeout 60s bash -c \"until kubectl -n metallb-system get deploy/controller --output jsonpath='{.status.readyReplicas}' | grep 1; do : ; done\""
        ssh_con.execute_commands([command])
        enable_bgp(name, config, ssh_con)


@fdo_infra.command(name="server-join")
@click.argument("name")
@click.argument("server-node")
@click.pass_context
def server_join(ctx, name: str, server_node: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logger.debug(config)

    server_node_private_ip = layer2_ipv4(server_node, config)

    logger.info(f"connecting to {server_node} to fetch credentials")
    with RemoteClient(server_node, config) as ssh_con:
        command = "sudo cat /var/lib/rancher/k3s/server/token"
        status, stdout, stderr = ssh_con.execute_command(command)
        token = stdout.read().rstrip().decode()

    logger.info(f"connecting to {name}")
    with RemoteClient(name, config) as ssh_con:
        setup_server(name, config, ssh_con, False)

        # install k3s
        command = f"""curl -sfL https://get.k3s.io | \
                K3S_TOKEN={token} INSTALL_K3S_CHANNEL={config['k3s']['channel']} sh -s - server \
                --server https://{server_node_private_ip}:6443"""

        ssh_con.execute_commands([command])

        enable_bgp(name, config, ssh_con)


@fdo_infra.command(name="join")
@click.argument("name")
@click.argument("server-node")
@click.pass_context
def join(ctx, name: str, server_node: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logger.debug(config)

    server_node_private_ip = layer2_ipv4(server_node, config)

    logger.info(f"connecting to {server_node} to get a token")
    with RemoteClient(server_node, config) as ssh_con:
        status, stdout, stderr = ssh_con.execute_command("sudo k3s token create")
        token = stdout.read().strip().decode()

        enable_bgp(name, config, ssh_con)

    logger.info(f"connecting to {name}")
    with RemoteClient(name, config) as ssh_con:
        setup_server(name, config, ssh_con, True)

        # install k3s
        command = f"""curl -sfL https://get.k3s.io | \
                K3S_TOKEN={token} INSTALL_K3S_CHANNEL={config['k3s']['channel']} sh -s - agent \
                --server https://{server_node_private_ip}:6443"""

        ssh_con.execute_commands([command])


def get_kilo_endpoint(ssh_con):
    status, stdout, stderr = ssh_con.execute_command(
        "sudo kubectl get node -o jsonpath='{.items[?(@.metadata.annotations.kilo\.squat\.ai/wireguard-ip!=\"\")].metadata.name}'"
    )
    kilo_endpoint = stdout.read().strip().decode()

    status, stdout, stderr = ssh_con.execute_command(
        f"sudo kubectl get node {kilo_endpoint} -o jsonpath='{{.metadata.annotations.kilo\.squat\.ai/wireguard-ip}}'"
    )
    kilo_cidr = stdout.read().strip().decode()

    return kilo_endpoint, kilo_cidr


def kubectl_apply_str(ssh_con, data):
    with tempfile.NamedTemporaryFile(delete=False) as fp:
        fp.write(bytes(data, "utf8"))

    tmpfile = fp.name
    ssh_con.bulk_upload([tmpfile], tmpfile)
    os.unlink(tmpfile)
    ssh_con.execute_commands([f"sudo kubectl apply -f {tmpfile}", f"rm {tmpfile}"])


def kubectl_apply_kustomize_str(ssh_con, data):
    with tempfile.NamedTemporaryFile(delete=False) as fp:
        fp.write(bytes(data, "utf8"))

    # we need a remote temporary dir...
    tempdir = tempfile.mkdtemp(prefix="kustomize-", dir=".")
    os.rmdir(tempdir)

    tempdir = Path(tempdir).name

    tmpfile = fp.name
    ssh_con.bulk_upload([tmpfile], tmpfile)
    os.unlink(tmpfile)
    ssh_con.execute_commands(
        [
            f"mkdir {tempdir}",
            f"mv {tmpfile} {tempdir}/kustomization.yaml",
            f"sudo kubectl apply -k {tempdir}",
            f"rm -rf {tempdir}",
        ]
    )


def deploy_kilo(ssh_con, server):
    kilo_kustomize = do_gomplate("gitlab-k3s-provision/kilo_kustomization.yaml")

    with tempfile.NamedTemporaryFile(delete=False) as fp:
        fp.write(bytes(kilo_kustomize, "utf8"))

    tmpfile = fp.name
    ssh_con.bulk_upload([tmpfile], tmpfile)
    os.unlink(tmpfile)
    ssh_con.execute_commands(
        [
            f"kubectl annotate node {server} kilo.squat.ai/leader=true",
            "mkdir -p /tmp/kilo",
            f"mv {tmpfile} /tmp/kilo/kustomization.yaml",
            "kustomize build /tmp/kilo/ > /tmp/kilo/kilo_manifest.yaml",
            "sudo kubectl apply -f /tmp/kilo/kilo_manifest.yaml",
            "rm -rf /tmp/kilo/",
        ]
    )


@fdo_infra.command(name="add-peer")
@click.option(
    "--server",
    help="one of the control plane server name",
    default="fdo-k3s-server-1",
)
@click.argument("name")
@click.pass_context
def add_peer(ctx, name: str, server: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logger.debug(config)

    # generate a public/private wg key pair
    with subprocess.Popen(["wg", "genkey"], stdout=subprocess.PIPE) as proc:
        assert proc.stdout is not None
        private_key = proc.stdout.read().rstrip()

    with subprocess.Popen(
        ["wg", "pubkey"], stdout=subprocess.PIPE, stdin=subprocess.PIPE
    ) as proc:
        public_key, err = proc.communicate(input=private_key)
        public_key = public_key.rstrip()

    logger.info(f"connecting to {server}")
    with RemoteClient(server, config) as ssh_con:
        # list all registered peers to get the allocated IPs
        status, stdout, stderr = ssh_con.execute_command(
            "sudo kubectl get -A peers.kilo.squat.ai -o json"
        )
        try:
            peers = json.load(stdout)
        except json.JSONDecodeError:
            # kilo not installed
            deploy_kilo(ssh_con, server)
            peers = {"items": []}

        cur_ips = []
        for item in peers["items"]:
            if item["metadata"]["name"] == name:
                logger.error(f"peer {name} is already used, aborting")
                sys.exit(1)
            cur_ips.extend(item["spec"]["allowedIPs"])

        kilo_endpoint, kilo_cidr = get_kilo_endpoint(ssh_con)
        kilo_public_ip = public_ipv4(kilo_endpoint, config)
        kilo_private_ip = layer2_ipv4(kilo_endpoint, config)
        kilo_address = kilo_cidr.split("/")[0]

        cur_ips.append(ipaddress.IPv4Address(kilo_address))

        if server != kilo_endpoint:
            logger.info(f"connecting to {kilo_endpoint}")
            with RemoteClient(
                kilo_endpoint,
                config,
            ) as kilo_ssh_con:
                # Retrieve the public key of the server
                status, stdout, stderr = kilo_ssh_con.execute_command(
                    "sudo wg show kilo0 public-key"
                )
                server_key = stdout.read().rstrip()
        else:
            # Retrieve the public key of the server
            status, stdout, stderr = ssh_con.execute_command(
                "sudo wg show kilo0 public-key"
            )
            server_key = stdout.read().rstrip()

        # Find a non-assigned IP in the kilo range
        assigned_ip = None
        net = ipaddress.ip_network(kilo_cidr, strict=False)
        while assigned_ip is None:
            test_ip = random.choice(list(net.hosts()))
            if test_ip not in cur_ips:
                assigned_ip = test_ip

        # write the kilo peer config
        kubectl_apply_str(
            ssh_con,
            f"""---
apiVersion: kilo.squat.ai/v1alpha1
kind: Peer
metadata:
  name: {name}
spec:
  allowedIPs:
  - {assigned_ip}/32
  publicKey: {public_key.decode()}
  persistentKeepalive: 10""",
        )

        # write the wireguard config file to stdout
        print(
            f"""[Interface]
Address = {assigned_ip}
PrivateKey = {private_key.decode()}
MTU = 1350

[Peer]
AllowedIPs = {config["k3s"]["cluster_cidr"]}, {config["k3s"]["service_cidr"]}, {kilo_private_ip}/32, {kilo_address}/32
Endpoint = {kilo_public_ip}:51821
PersistentKeepalive = 10
PublicKey = {server_key.decode()}"""
        )


@fdo_infra.command(name="make-kubeconfig")
@click.option(
    "--server",
    help="one of the control plane server name",
    default="fdo-k3s-server-1",
)
@click.option(
    "--namespace",
    help="if set, the user will only have access to that namespace",
    default=None,
)
@click.argument("user")
@click.pass_context
def make_kubeconfig(ctx, user: str, server: str, namespace):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logger.debug(config)

    if namespace is not None:
        admin_name = f"admin-{namespace}-{user}"
    else:
        admin_name = f"admin-{user}"

    token_name = f"{admin_name}-token-{uuid.uuid4().hex[:6]}"

    logger.info(f"connecting to {server}")
    with RemoteClient(
        server,
        config,
    ) as ssh_con:
        commands = []
        if namespace is None:
            namespace = "kube-system"
            commands.extend(
                [
                    f"sudo kubectl -n kube-system create serviceaccount {admin_name}",
                    f"sudo kubectl create clusterrolebinding add-on-cluster-admin-{admin_name} \
                        --clusterrole=cluster-admin \
                        --serviceaccount=kube-system:{admin_name}",
                ]
            )
            ssh_con.execute_commands(commands)
        else:
            kubectl_apply_str(
                ssh_con,
                f"""---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {admin_name}
  namespace: {namespace}
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {namespace}-admin
  namespace: {namespace}
rules:
- apiGroups: ["", "extensions", "apps"]
  resources: ["*"]
  verbs: ["*"]
- apiGroups: ["batch"]
  resources:
  - jobs
  - cronjobs
  verbs: ["*"]

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {namespace}-{admin_name}-view
  namespace: {namespace}
subjects:
- kind: ServiceAccount
  name: {admin_name}
  namespace: {namespace}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: {namespace}-admin
""",
            )

        # Since k8s 1.22, the token is not created automatically
        kubectl_apply_str(
            ssh_con,
            f"""---
apiVersion: v1
kind: Secret
metadata:
  name: {token_name}
  namespace: {namespace}
  annotations:
    kubernetes.io/service-account.name: "{admin_name}"
type: kubernetes.io/service-account-token
""",
        )

        status, stdout, stderr = ssh_con.execute_command(
            f"sudo kubectl -n {namespace} get secret {token_name} -o jsonpath='{{.data.token}}'"
        )
        encoded_token = stdout.read().strip()
        token = base64.decodebytes(encoded_token).decode("utf8")

        status, stdout, stderr = ssh_con.execute_command(
            "sudo grep certificate-authority-data /etc/rancher/k3s/k3s.yaml | cut -d ':' -f 2 | xargs"
        )
        cluster_cert = stdout.read().strip()

        kilo_endpoint, kilo_cidr = get_kilo_endpoint(ssh_con)
        kilo_private_ip = layer2_ipv4(kilo_endpoint, config)

        k8s_cert_fp = tempfile.NamedTemporaryFile(delete=False)
        k8s_cert_fp.write(base64.decodebytes(cluster_cert))
        k8s_cert_fp.close()

        kube_config_fp = tempfile.NamedTemporaryFile(delete=False)
        kube_config_fp.close()

        def run_with_kubectx(cmd, stdout=subprocess.DEVNULL):
            subprocess.run(
                shlex.split(cmd),
                env={"KUBECONFIG": kube_config_fp.name, "PATH": os.getenv("PATH")},
                stdout=stdout,
            ).check_returncode()

        try:
            run_with_kubectx(
                f"kubectl config set-credentials {admin_name} --token={token}"
            )
            run_with_kubectx(
                f"""kubectl config set-cluster {server} \
                    --server=https://{kilo_private_ip}:6443 \
                    --certificate-authority={k8s_cert_fp.name} \
                    --embed-certs=true"""
            )
            run_with_kubectx(
                f"""kubectl config set-context \
                    admin@{server} \
                    --cluster {server} \
                    --user {admin_name}"""
            )
            run_with_kubectx(f"kubectl config use-context admin@{server}")

            logger.info("the following is your new kubeconfig file")
            logger.info("---")

            run_with_kubectx(
                "kubectl config view --minify=true --raw=true", stdout=None
            )
        finally:
            os.unlink(k8s_cert_fp.name)
            os.unlink(kube_config_fp.name)


def main(*args, **kwargs):
    fdo_infra(obj={}, *args, **kwargs)


if __name__ == "__main__":
    main()

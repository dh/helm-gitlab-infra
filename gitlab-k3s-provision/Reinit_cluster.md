On the equinix console, "reinstall" all nodes so that they have their disks flushed.

`./fdo-infra.py cluster-init fdo-k3s-server-10`

Wait for the server to reboot

`/fdo-infra.py server-join fdo-k3s-server-11 fdo-k3s-server-10`
`/fdo-infra.py server-join fdo-k3s-server-12 fdo-k3s-server-10`

Upload the broker information to server-10

`scp -P 2222 broker-info.subm  core@IP_OF_SERVER_10:`

On server-10:

`sudo subctl --kubeconfig /etc/rancher/k3s/k3s.yaml join ~core/broker-info.subm --clusterid equinix-dc --natt=false --clustercidr=172.21.0.0/16`

On server-12:

`for ip in IPS... ; do sudo firewall-cmd --zone=vxlan --add-source=${ip}/32; done`
`for ip in IPS... ; do sudo firewall-cmd --permanent --zone=vxlan --add-source=${ip}/32; done`
`sudo kubectl label node fdo-k3s-server-12 submariner.io/gateway=true`

Now join the large- servers

`./fdo-infra.py join fdo-k3s-large-13 fdo-k3s-server-10`
`./fdo-infra.py join fdo-k3s-large-14 fdo-k3s-server-10`
`./fdo-infra.py join fdo-k3s-large-15 fdo-k3s-server-10`

Add a local kilo connection and a kubeconfig

`./fdo-infra.py add-peer $USER-$HOSTNAME --server fdo-k3s-server-10 > ~/fdo/fdo-k3s-dc.conf`
`chmod 600 ~/fdo/fdo-k3s-dc.conf`
`sudo wg-quick up ~/fdo/fdo-k3s-dc.conf`

`./fdo-infra.py make-kubeconfig bentiss --server fdo-k3s-server-10 > ~/.kube/fdo-k3s-server-dc.yaml`
`chmod 600 ~/.kube/fdo-k3s-server-dc.yaml`

Test if the cluster is working fine

`kubectl --kubeconfig ~/.kube/fdo-k3s-server-dc.yaml get nodes`

Deploy kube-vip

`cd ~/Src/helm-gitlab-infra/gitlab-k3s-provision/deploy`
`KUBECONFIG=~/.kube/fdo-k3s-server-dc.yaml helmfile -e equinix-DC -l chart=kube-vip -i apply`

Deploy the ingress nginx

`cd ~/Src/helm-gitlab-deployment`
`KUBECONFIG=~/.kube/fdo-k3s-server-dc.yaml helmfile -e equinix-DC -l chart=ingress-nginx -i apply`

Check that the svc LB are correctly assigned and working

`kubectl --kubeconfig ~/.kube/fdo-k3s-server-dc.yaml get -A svc`

Deploy rook-ceph

`cd ~/Src/helm-gitlab-infra/gitlab-k3s-provision/deploy/storage`

Edit rook-ceph-cluster.yaml, comment out all of cephObjectStores and add instead
`cephObjectStores = []`

`KUBECONFIG=~/.kube/fdo-k3s-server-dc.yaml helmfile -e equinix-DC -l chart=rook-ceph -i apply`
`KUBECONFIG=~/.kube/fdo-k3s-server-dc.yaml helmfile -e equinix-DC -l chart=rook-ceph-cluster -i apply`

Deploy the secrets for the realm (see https://rook.io/docs/rook/latest/Storage-Configuration/Object-Storage-RGW/ceph-object-multisite/#getting-realm-access-key-and-secret-key)

Now deploy ceph-cluster with only *one* realm:

Ensure on the old cluster that the rgw service is exported through subctl:
`subctl export service -n rook-ceph rook-ceph-rgw-fdo-opa`

In ceph-cluster-kustomize:
- enable only the realm
- deploy and ensure that there is no errors on the ceph operator pod
- then enable the zonegroup
- deploy and ensure that there is no errors on the ceph operator pod
- then enable the zone, making sure it has a new name different from the original cluster
- deploy and ensure that there is no errors on the ceph operator pod

In rook-toolbox:
```
ZONE=harbor-dc
for kind in control meta log buckets.non-ec buckets.index otp ; \
  do ceph osd pool set fdo-${ZONE}.rgw.$kind pg_num 8; \
  ceph osd pool set fdo-${ZONE}.rgw.$kind pg_num_min 8; done
ceph osd pool set fdo-${ZONE}.rgw.buckets.data pg_num 32
ceph osd pool set fdo-${ZONE}.rgw.buckets.data pg_num_min 32
```

Note:
- for harbor -> buckets.data pg_num = 32
- for opa -> buckets.data pg_num = 64
- for s3 -> buckets.data pg_num = 512

In rook-ceph-cluster:
- enable the matching object store, ensuring it has a different name from the original cluster

replication should happen:
`kubectl --kubeconfig ~/.kube/fdo-k3s-server-dc.yaml -n rook-ceph exec -it deploy/rook-ceph-tools -- bash`
`radosgw-admin  --rgw-realm=fdo-opa sync status ; ceph df`

Outside of the ceph-tools pod, export the rgw service through subctl:
`sudo subctl --kubeconfig /etc/rancher/k3s/k3s.yaml export service -n rook-ceph rook-ceph-rgw-fdo-opa-dc`

When the metadata replication finishes, in the ceph-tools pod of the new server:
`kubectl --kubeconfig ~/.kube/fdo-k3s-server-dc.yaml -n rook-ceph exec -it deploy/rook-ceph-tools -- bash`
`radosgw-admin  --rgw-realm=fdo-opa period update --commit`

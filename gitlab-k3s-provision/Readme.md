# Description

This will create a k3s cluster on equinix metal with the following:
- use of `c3.medium.x86` and `s3.xlarge.x86` devices
- HA control plane
- multiple agents
- a layer 2 vlan for control plane communication (not exposed to the outside world)
- inter-agent communication through `vxlan`
- `kilo` as a VPN server to add peers to configure the cluster
- `external-DNS` to automatically populate the DNS entries with the local IPs
- `rook+ceph` to provide a CSI with the local disks on the server/agents

# Prerequesites

## Connect the local machine to the cluster:

```bash
./fdo-infra.py --server fdo-k3s-server-11 add-peer $USER-$(hostname) > fdo-k3s.conf

chmod 600 fdo-k3s.conf
wg-quick up ./fdo-k3s.conf

# fetch a k3s config file
mkdir -p ~/.kube/
./fdo-infra.py make-kubeconfig $USER-$(hostname) --server fdo-k3s-server-11 > ~/.kube/k3s.yaml

KUBECONFIG=~/.kube/k3s.yaml kubectl get -A pods
```

# Access the rook+ceph dashboard

The address of the dashboard can be retrieve with the following:

```bash
KUBECONFIG=~/.kube/k3s.yaml kubectl -n rook-ceph get svc rook-ceph-mgr-dashboard

# get the password
KUBECONFIG=~/.kube/k3s.yaml kubectl -n rook-ceph get secrets \
    rook-ceph-dashboard-password -o jsonpath='{.data.password}' \
    | base64 -d
```

Then open a browser to the `CLUSTER-IP` shown above.

# Add an agent to the cluster
```bash
export METAL_PROJECT_ID=@REPLACE_ME@
export METAL_PROJECT_TOKEN=@REPLACE_ME@
export METAL_DEVICE_NAME=fdo-k3s-large-14

./gitlab-k3s-packet.sh

# wait for the host to be ready

# attach the agent to the cluster
./fdo-infra.py join $METAL_DEVICE_NAME fdo-k3s-server-12

# watch the pods getting ready
KUBECONFIG=~/.kube/k3s.yaml kubectl get -A pods -o wide -w
```

# Re-deploy the k3s server
Note this will create an entire new cluster

```bash
export METAL_PROJECT_ID=@REPLACE_ME@
export METAL_PROJECT_TOKEN=@REPLACE_ME@

METAL_DEVICE_NAME=fdo-k3s-server-1 ./gitlab-k3s-packet.sh
METAL_DEVICE_NAME=fdo-k3s-server-2 ./gitlab-k3s-packet.sh
METAL_DEVICE_NAME=fdo-k3s-server-3 ./gitlab-k3s-packet.sh

# wait for the hosts to be ready
./fdo-infra.py cluster-init fdo-k3s-server-1

# wait a few seconds for kilo to be applied
./fdo-infra.py add-peer --server fdo-k3s-server-1 $USER-$(hostname) > fdo-k3s.conf

chmod 600 fdo-k3s.conf
wg-quick up ./fdo-k3s.conf

./fdo-infra.py server-join fdo-k3s-server-2 fdo-k3s-server-1
./fdo-infra.py server-join fdo-k3s-server-3 fdo-k3s-server-1
```

#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Daniel Stone <daniel@fooishbar.org>
from __future__ import annotations

import click
import os
import requests
from yaml import safe_dump as yaml_safe_dump
from yaml import SafeDumper
from pathlib import Path
from sys import stdout


# use literal rather than quoted encoding for block strings
# https://stackoverflow.com/a/33300001
def str_presenter(dumper, data):
    if os.linesep in data:  # check for multiline string
        return dumper.represent_scalar("tag:yaml.org,2002:str", data, style="|")
    return dumper.represent_scalar("tag:yaml.org,2002:str", data)


SafeDumper.add_representer(str, str_presenter)

local_path = Path(__file__).parent


def get_local_file(filename):
    return (local_path / filename).read_text()


def get_remote_uri(uri):
    get = requests.get(uri)
    if not get.ok:
        get.raise_for_status()
    return get.text


cloud_init = {
    "package-update": True,
    "package-upgrade": True,
    "package_reboot_if_required": True,
    "apt": {"preserve_sources_list": True, "sources": {}},
    "packages": [
        # ??
        "patch",
        # network
        "wireguard",
        "firewalld",
        # daniels
        "zsh",
        "vim",
        # bentiss
        "neovim",
        # misc
        "jq",
        "mdadm",
        "parted",
    ],
    "runcmd": [],
    "write_files": [
        {
            "path": "/etc/kernel-img.conf",
            "owner": "root:root",
            "content": "do_symlinks=Yes\nimage_dest=/boot",
        },
        {
            "path": "/etc/podman-gc-exclude",
            "owner": "root:root",
            "content": get_local_file("podman-gc-exclude"),
        },
        {
            "path": "/etc/systemd/system/serial-getty@ttyS1.service.d/autologin-root.conf",
            "owner": "root:root",
            "content": """
[Service]
# Override Execstart in main unit
ExecStart=
# Add new Execstart with `-` prefix to ignore failure`
ExecStart=-/usr/sbin/agetty --autologin root --noclear %I $TERM"""
        }
    ],
}


def add_apt_repository(
    name: str, base_uri: str,
    distribution: list[str], key_uri: str,
) -> None:
    key_path = f"/usr/share/keyrings/{name}.asc"
    cloud_init["apt"]["sources"][name] = {
        "source": f"deb [signed-by={key_path}] {base_uri} {' '.join(distribution)}"
    }
    cloud_init["write_files"].append(
        {
            "path": key_path,
            "owner": "root:root",
            "permissions": "'0644'",
            "content": get_remote_uri(key_uri),
        }
    )


def add_dedicated_storage(gitlab_runner_arch):
    if gitlab_runner_arch == "x86-64":
        cloud_init["runcmd"].extend(
            [
                # configure RAID for Docker storage
                "mkdir -p /run/cloudinit",
                """lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and (.name | startswith("nvme")))) | map_values("/dev/" + .name) | join(" ")' > /run/cloudinit/FREE_HDD""",
                """lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and (.name | startswith("nvme")))) | map_values("/dev/" + .name + "p1") | join(" ")' >> /run/cloudinit/FREE_HDD_PART""",
                """lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and (.name | startswith("nvme")))) | length' > /run/cloudinit/COUNT_HDD""",
                "echo FREE_HDD $(cat /run/cloudinit/FREE_HDD)",
                "echo FREE_HDD_PART $(cat /run/cloudinit/FREE_HDD_PART)",
                "echo COUNT_HDD $(cat /run/cloudinit/COUNT_HDD)",
                "for DEV in $(cat /run/cloudinit/FREE_HDD) ; do echo create partition on $DEV; echo label: gpt | sfdisk $DEV; echo type=R | sfdisk $DEV; done",
                "mdadm --create --verbose /dev/md0 --level=0 --raid-devices=$(cat /run/cloudinit/COUNT_HDD) $(cat /run/cloudinit/FREE_HDD_PART)",
                "mkfs.ext4 -F /dev/md0",
                "echo 'UUID=\"'$(blkid -s UUID -o value /dev/md0)'\" /var/lib/containers ext4 defaults 0 0' | tee -a /etc/fstab",
                "mkdir -p /var/lib/containers",
                "mount -a",
            ]
        )


def add_gitlab_runner(
    instance_name,
    registration_token,
    concurrent,
    gitlab_tags,
    maybe_untagged,
    gitlab_runner_arch,
):
    cloud_init["write_files"].append(
        {
            "path": "/etc/containers/containers.conf",
            "owner": "root:root",
            "permissions": "0644",
            "content": get_local_file("containers.conf"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/systemd/system/gitlab-runner.service.d/kill.conf",
            "owner": "root:root",
            "content": get_local_file("gitlab-runner-systemd-kill.conf"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/modprobe.d/vsock.conf",
            "owner": "root:root",
            "content": """
        blacklist vmw_vmci
        blacklist vmw_vsock_vmci_transport
        blacklist vmw_vsock_virtio_transport_common
        """,
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/modules-load.d/vsock.conf",
            "owner": "root:root",
            "content": "vhost_vsock",
        }
    )
    add_apt_repository(
        name="gitlab-runner",
        base_uri="https://packages.gitlab.com/runner/gitlab-runner/debian",
        distribution=["$RELEASE", "main"],
        key_uri="https://packages.gitlab.com/runner/gitlab-runner/gpgkey",
    )

    cloud_init["packages"].extend(["gitlab-runner"])
    # container runtime
    cloud_init["packages"].extend(
        ["podman", "slirp4netns", "fuse-overlayfs", "uidmap", "tini", "cgroupfs-mount"]
    )
    # needed for helper-image bootstrapping
    cloud_init["packages"].extend(
        ["binutils", "binutils-common", "cdebootstrap", "gettext-base"]
    )
    if "aarch64" in gitlab_tags:
        cloud_init["packages"].extend(["binutils-aarch64-linux-gnu"])
    else:
        cloud_init["packages"].extend(["binutils-x86-64-linux-gnu"])
    # support qemu for cross-arch building
    cloud_init["packages"].extend(["binfmt-support", "qemu-user-static"])

    if maybe_untagged:
        gitlab_untagged = "--run-untagged"
    else:
        gitlab_untagged = ""

    # pull static curl from https://github.com/moparisthebest/static-curl
    cloud_init["runcmd"].append("mkdir -p /var/host/bin")
    curl_url = f"https://gitlab.freedesktop.org/api/v4/projects/freedesktop%2Fhelm-gitlab-infra/packages/generic/curl/7.88.1/curl-"
    jq_url = f"https://gitlab.freedesktop.org/api/v4/projects/freedesktop%2Fhelm-gitlab-infra/packages/generic/jq/jq-1.6-159-gcff5336/jq-"
    if gitlab_runner_arch == "x86-64":
        curl_url += "amd64"
        jq_url += "amd64"
    else:
        curl_url += "aarch64"
        jq_url += "aarch64"

    gating_script = "https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/runner-gating/runner-gating.sh"
    curl_cmd = "/host/bin/curl -s -L --cacert /host/ca-certificates.crt --retry 4 -f --retry-delay 60"

    register_command = f"--name {instance_name}"
    register_command += f" --non-interactive"
    register_command += f" --limit {concurrent}"
    register_command += f" --request-concurrency 2"
    register_command += f" --executor docker"
    register_command += f" --docker-host unix:///run/podman/podman.sock"
    register_command += f" --docker-pull-policy if-not-present"
    register_command += f" --docker-image alpine:latest"
    register_command += f" --docker-privileged"
    register_command += f" --docker-devices /dev/kvm"
    register_command += f' --docker-volumes "/var/cache/gitlab-runner/cache:/cache"'
    register_command += f' --docker-volumes "/var/host:/host:ro"'
    register_command += f" --custom_build_dir-enabled=true"
    register_command += f" --registration-token {registration_token}"
    register_command += f' --env "DOCKER_TLS_CERTDIR="'
    register_command += f' --env "FDO_CI_CONCURRENT={concurrent}"'
    if gitlab_tags:
        register_command += f" --tag-list {gitlab_tags}"
    register_command += f" {gitlab_untagged}"
    register_command += f" --docker-tmpfs /tmp:rw,nosuid,nodev,exec,mode=1777"
    register_command += f' --docker-extra-hosts "ssh.tmate.io:192.0.2.1"'
    register_command += f" --url https://gitlab.freedesktop.org"
    register_command += f' --pre-get-sources-script "{curl_cmd} {gating_script} | sh -s -- pre_get_sources_script"'
    register_command += f' --pre-build-script "{curl_cmd} {gating_script}  | sh"'

    cloud_init["runcmd"].extend(
        [
            "mkdir -p /var/host/bin",
            f"curl -L -o /var/host/bin/curl {curl_url}",
            "chmod +x /var/host/bin/curl",
            f"curl -L -o /var/host/bin/jq {jq_url}",
            "chmod +x /var/host/bin/jq",
            "cp /etc/ssl/certs/ca-certificates.crt /var/host/",
            "systemctl enable --now podman.socket",
            "systemctl enable --now gitlab-runner.service",
            f"mkdir -p /var/cache/gitlab-runner/cache",
            f"mkdir /etc/gitlab-runner",
            f'echo "concurrent = {concurrent}" > /etc/gitlab-runner/config.toml',
            """echo "listen_address = "\\"$(ip -j -4 addr show | jq -r '.[] | (.addr_info[] | .local) | select(startswith("10."))')":3807\\"" >> /etc/gitlab-runner/config.toml""",
            f"gitlab-runner register {register_command}",
        ]
    )


def add_docker_dfs(gitlab_runner_arch):
    cloud_init["write_files"].append(
        {
            "path": "/usr/local/sbin/docker-free-space",
            "owner": "root:root",
            "permissions": "0755",
            "content": get_local_file("docker-free-space.py"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/systemd/system/docker-free-space.service",
            "owner": "root:root",
            "content": get_local_file("docker-free-space.service"),
        }
    )
    cloud_init["runcmd"].append("systemctl daemon-reload")
    cloud_init["runcmd"].append("systemctl enable --now docker-free-space.service")
    cloud_init["packages"].extend(
        [
            "python3-docker",
            "python3-click",
            "python3-parse",
            "python3-git",
            "python3-yaml",
        ]
    )


def add_podman_dfs(gitlab_runner_arch):
    cloud_init["write_files"].append(
        {
            "path": "/usr/local/sbin/podman-free-space",
            "owner": "root:root",
            "permissions": "0755",
            "content": get_local_file("podman-free-space.py"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/systemd/system/podman-free-space.service",
            "owner": "root:root",
            "content": get_local_file("podman-free-space.service"),
        }
    )
    cloud_init["packages"].extend(
        [
            "python3-pip",
            "python3-click",
            "python3-parse",
            "python3-git",
            "python3-yaml",
        ]
    )
    # pip doesn't let us install system wide packages outside of venv
    cloud_init["runcmd"].append("pip3 install --break-system-packages podman")
    cloud_init["runcmd"].append("systemctl daemon-reload")
    cloud_init["runcmd"].append("systemctl enable --now podman-free-space.service")


@click.command()
@click.option(
    "--instance-name", type=click.STRING, required=True, help="Name for this instance"
)
@click.option(
    "--gitlab-runner-registration-token",
    type=click.STRING,
    required=True,
    help="gitlab-runner registration token",
)
@click.option(
    "--gitlab-runner-concurrent",
    type=click.INT,
    required=True,
    help="Number of gitlab-runner jobs to run concurrently",
)
@click.option(
    "--gitlab-runner-tags",
    type=click.STRING,
    default="",
    help="gitlab-runner tag list (comma-separated)",
)
@click.option(
    "--gitlab-runner-untagged/--gitlab-runner-only-tagged",
    default=False,
    help="Whether to accept GitLab CI jobs with no specified tags",
)
@click.option(
    "--gitlab-runner-arch",
    type=click.STRING,
    help="gitlab-runner arch (x86-64 or aarch64)",
)
def main(
    instance_name,
    gitlab_runner_registration_token,
    gitlab_runner_concurrent,
    gitlab_runner_tags,
    gitlab_runner_untagged,
    gitlab_runner_arch,
):
    add_dedicated_storage(gitlab_runner_arch)
    add_gitlab_runner(
        instance_name,
        gitlab_runner_registration_token,
        gitlab_runner_concurrent,
        gitlab_runner_tags,
        gitlab_runner_untagged,
        gitlab_runner_arch,
    )
    add_podman_dfs(gitlab_runner_arch)

    stdout.write("#cloud-config\n")
    # these options make the result more human readable
    yaml_safe_dump(
            cloud_init, stdout,
            indent=4, width=9999, default_flow_style=False
    )
    stdout.write("\n")


if __name__ == "__main__":
    main()

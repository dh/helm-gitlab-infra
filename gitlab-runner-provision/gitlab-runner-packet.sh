#!/bin/sh

set -e
set -u
set -o pipefail

# you must set:
#  - $METAL_PROJECT_ID to the fd.o CI Packet project
#  - $RUNNER_ARCH to x86-64 or aarch64
#  - $METAL_DEVICE_NAME to the desired device name (e.g. fdo-packet-5) (fallbacks on PACKET_DEVICE_NAME)
#  - $GITLAB_RUNNER_REG_TOKEN to the shared-runner registration token from https://gitlab.freedesktop.org/admin/runners

METAL_DEVICE_NAME=${METAL_DEVICE_NAME:-$PACKET_DEVICE_NAME}

SCRIPT_DIR=$(dirname "$(realpath "${BASH_SOURCE[0]}")")

case "$RUNNER_ARCH" in
  x86-64)
    METAL_PLAN="m3.large.x86"
    METAL_METRO="dc"
    METAL_TAGS="ci,ci-x86"
    GITLAB_RUNNER_CONCURRENT=8
    GITLAB_RUNNER_TAGS="kvm,packet.net"
    GITLAB_RUNNER_UNTAGGED="--gitlab-runner-untagged"
    ;;
  aarch64)
    METAL_PLAN="c3.large.arm64"
    METAL_METRO="dc"
    METAL_TAGS="ci,ci-aarch64"
    GITLAB_RUNNER_CONCURRENT=16
    GITLAB_RUNNER_TAGS="aarch64,kvm-aarch64"
    GITLAB_RUNNER_UNTAGGED=""
    ;;
  *)
    echo "unknown arch $RUNNER_ARCH"
    exit 1
    ;;
esac

if [ "$(metal device get --project-id $METAL_PROJECT_ID -o json | jq -r '.[] | select(.hostname == "'$METAL_DEVICE_NAME'") | .hostname')" == $METAL_DEVICE_NAME ]; then
	echo "Device name $METAL_DEVICE_NAME already taken"
	exit 1
fi

CUSTOM_DATA=$(env -i \
  GITLAB_RUNNER_REG_TOKEN=$GITLAB_RUNNER_REG_TOKEN \
  GITLAB_RUNNER_CONCURRENT=$GITLAB_RUNNER_CONCURRENT \
  GITLAB_RUNNER_TAGS=$GITLAB_RUNNER_TAGS \
  GITLAB_RUNNER_UNTAGGED=$GITLAB_RUNNER_UNTAGGED \
  jq -nc --argjson vlans '[]' \
  '{vlans: $vlans, environment: $ENV}'
)

CLOUD_INIT="$(./generate-cloud-init.py \
                  --instance-name $METAL_DEVICE_NAME \
                  --gitlab-runner-registration-token $GITLAB_RUNNER_REG_TOKEN \
                  --gitlab-runner-concurrent $GITLAB_RUNNER_CONCURRENT \
                  --gitlab-runner-tags $GITLAB_RUNNER_TAGS \
                  $GITLAB_RUNNER_UNTAGGED \
                  --gitlab-runner-arch $RUNNER_ARCH)"

DEVICE_ID="$(metal device create --project-id $METAL_PROJECT_ID \
                                 --hostname $METAL_DEVICE_NAME \
                                 --plan $METAL_PLAN \
                                 --metro $METAL_METRO \
                                 --tags $METAL_TAGS \
                                 --operating-system debian_12 \
                                 --userdata "${CLOUD_INIT}" \
                                 --customdata "$CUSTOM_DATA" \
                                 --output json | jq -r .id)"

echo "Device $DEVICE_ID successfully created"

DEVICE_STATE=""

while ! [ "$DEVICE_STATE" = "active" ]; do
	sleep 10
	DEVICE_STATE="$(metal device get --id $DEVICE_ID -o json | jq -r .state)"
done

echo "Device $DEVICE_ID became active"


DEVICE_IP="$(metal device get --id $DEVICE_ID -o json | jq -r '.ip_addresses[] | select(.address_family == 4 and (.address | startswith("10.") | not)) | .address')"
echo "IP: $DEVICE_IP"

SOS_ADDR="${DEVICE_ID}@sos.$(metal device get --id $DEVICE_ID -o json | jq -r .facility.code).platformequinix.com"
echo "Remote console: $SOS_ADDR"

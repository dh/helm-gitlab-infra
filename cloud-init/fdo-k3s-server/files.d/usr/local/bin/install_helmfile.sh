#!/bin/bash

{{ $config := (tmpl.Exec "utils" .) | json -}}

set -xe

# download and unpack the various packages
curl -L {{ $config.install.kustomize }} | tar xvzf - -C /usr/local/bin kustomize
curl -L {{ $config.install.helm }} | tar xvzf - -C /usr/local/bin --strip-components 1 linux-amd64/helm
curl -L {{ $config.install.helmfile }} | tar xvzf - -C /usr/local/bin helmfile
HOME=/root helm plugin install https://github.com/databus23/helm-diff --version master

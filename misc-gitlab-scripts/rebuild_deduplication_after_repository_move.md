
If git repos are moved between shards, deduplication gets severed:
https://docs.gitlab.com/ee/development/git_object_deduplication.html#consequences

## NOTE:

we are playing with the DB, use with caution!

# Set up environment

You need access to the cluster itself with `kubectl`.

You need an API token exported in `$GITLAB_TOKEN`.

Then you need to retrieve the PostgresQL password for the following commands.

```bash
export PGPASSWORD=$(kubectl get secrets gitlab-prod-postgresql-password -o 'go-template={{index .data "postgresql-password"}}' | base64 -d)
```

# Ensure the corresponding pool is on the correct shard

```bash
kubectl exec -t gitlab-prod-postgresql-0 -c gitlab-prod-postgresql-postgresql -- env PGPASSWORD=$PGPASSWORD \
    psql -U gitlab gitlab_production -c \
        "select pool_repositories.id as pool_id, pool_repository_id as project_pool_id, shard_id, shards.name as shard_name, repository_storage as project_actual_shard, disk_path, source_project_id, namespaces.path, projects.name, state \
            from pool_repositories, shards, projects, namespaces \
                where projects.namespace_id = namespaces.id AND \
                projects.id=source_project_id AND \
                shards.id=shard_id \
            order by shard_id, namespaces.path, projects.name ASC;"
```

If `shard_name` and `project_actual_shard` are identical (i.e. project shard matches pool shard), `project_pool_id` should match `pool_id`. If not, then the repository pool will not get processed by `housekeeping`.

Note: this should not be done manually in most cases except right after moving the repository. If the repo has been moved previously and has been forked, a new repository pool should be already present.

## ensure `$project` is on the correct pool_repository_id:
```bash
# example with mesa, project id 176 and pool_id 17
kubectl exec -t gitlab-prod-postgresql-0 -c gitlab-prod-postgresql-postgresql -- env PGPASSWORD=$PGPASSWORD \
    psql -U gitlab gitlab_production -c \
        "UPDATE projects set pool_repository_id = 17 where id = 176;"
```

## trigger housekeeping on $SOURCE_PROJECT:
To ensure the pool ID is correctly set, we need to trigger a `housekeeping` jb on the project.

```bash
curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.freedesktop.org/api/v4/projects/176/housekeeping"
```

## enumerate all projects
```bash
kubectl exec -t gitlab-prod-postgresql-0 -c gitlab-prod-postgresql-postgresql -- env PGPASSWORD=$PGPASSWORD \
    psql -U gitlab gitlab_production -c \
        "select projects.id, pool_repository_id, repository_storage, namespaces.path, projects.name \
            from projects, namespaces \
                where projects.namespace_id = namespaces.id \
            order by namespaces.path, projects.name ASC;"
```

## enumerate all projects that are in 'gitaly-1' shard, and which should be part of a repository_pool
```bash
export SHARD=\'gitaly-1\' 
kubectl exec -t gitlab-prod-postgresql-0 -c gitlab-prod-postgresql-postgresql -- env PGPASSWORD=$PGPASSWORD \
    psql -U gitlab gitlab_production -c \
        "select projects.id, forked_from_project_id, pool_repository_id, pool_repositories.id as target_pool_id, repository_storage, namespaces.path, projects.name \
            from projects, namespaces, fork_network_members, pool_repositories \
                where projects.namespace_id = namespaces.id \
                    AND projects.id = fork_network_members.project_id \
                    AND pool_repositories.source_project_id = forked_from_project_id \
                    AND repository_storage = $SHARD \
                    AND forked_from_project_id IN ( select id from projects where repository_storage = $SHARD) \
            order by namespaces.path, projects.name ASC;"
```

This only gives valid pools/projects couples because the subquery ensures the source of the forks are in the same shard.

Here, `pool_repository_id` (if set) should be equal to `target_pool_id`.

## enumerate all projects that are in 'gitaly-1' shard, and which should be part of a repository_pool, but are not

Almost same query than previously, but with:
- json output
- only projects with `pool_repository_id` not set
- hashed storage path

```bash
export SHARD=\'gitaly-1\' 
kubectl exec -t gitlab-prod-postgresql-0 -c gitlab-prod-postgresql-postgresql -- env PGPASSWORD=$PGPASSWORD \
    psql -U gitlab gitlab_production -A -tc \
        "select json_agg(t) from (
            select projects.id, forked_from_project_id, pool_repository_id, pool_repositories.id as target_pool_id, repository_storage, namespaces.path, projects.name, project_repositories.disk_path \
                from projects, namespaces, fork_network_members, pool_repositories, project_repositories \
                    where projects.namespace_id = namespaces.id \
                        AND projects.id = fork_network_members.project_id \
                        AND projects.id = project_repositories.project_id \
                        AND repository_storage = $SHARD \
                        AND pool_repositories.source_project_id = forked_from_project_id \
                        AND pool_repository_id is null \
                        AND forked_from_project_id IN ( select id from projects where repository_storage = $SHARD) \
                order by namespaces.path, projects.name ASC) t;" | jq
```

## Now the magic happens

```bash
# Fetch the first item in the db
function get_one_project {
    kubectl exec -t gitlab-prod-postgresql-0 -c gitlab-prod-postgresql-postgresql -- env PGPASSWORD=$PGPASSWORD \
        psql -U gitlab gitlab_production -A -tc \
            "select json_agg(t) from (
                select projects.id, forked_from_project_id, pool_repository_id, pool_repositories.id as target_pool_id, repository_storage, namespaces.path, projects.name, project_repositories.disk_path \
                    from projects, namespaces, fork_network_members, pool_repositories, project_repositories \
                        where projects.namespace_id = namespaces.id \
                            AND projects.id = fork_network_members.project_id \
                            AND projects.id = project_repositories.project_id \
                            AND repository_storage = $SHARD \
                            AND pool_repositories.source_project_id = forked_from_project_id \
                            AND pool_repository_id is null \
                            AND forked_from_project_id IN ( select id from projects where repository_storage = $SHARD) \
                    order by namespaces.path, projects.name ASC LIMIT 1) t;"
}

# get how many jobs are currently busy
function get_busy {
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.freedesktop.org/api/v4/sidekiq/process_metrics" | jq -r '.processes | .[].busy'
}

# start houskeeping on a project
function housekeeping {
    PROJECT_ID=$1
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.freedesktop.org/api/v4/projects/$PROJECT_ID/housekeeping" | jq -r
}

inquire ()  {
  echo  -n "$1 [y*/n]? "
  read answer
  finish="-1"
  while [ "$finish" = '-1' ]
  do
    finish="1"
    if [ "$answer" = '' ];
    then
      answer="y"
    else
      case $answer in
        y | Y | yes | YES ) answer="y";;
        n | N | no | NO ) answer="n";;
        *) finish="-1";
           echo -n 'Invalid response -- please reenter:';
           read answer;;
       esac
    fi
  done
}

while : 
do
    PROJECT=$(get_one_project)
    
    if [[ x"$(echo $PROJECT | jq '.[].id')" == x"" ]]
    then
        break
    fi
    
    echo $PROJECT | jq

    inquire "continue with this project?"

    if [[ x"$answer" != x"y" ]]
    then
        break
    fi

    TARGET_POOL=$(echo $PROJECT | jq '.[].target_pool_id')
    PROJECT_ID=$(echo $PROJECT | jq '.[].id')
    kubectl exec -t gitlab-prod-postgresql-0 -c gitlab-prod-postgresql-postgresql -- env PGPASSWORD=$PGPASSWORD \
        psql -U gitlab gitlab_production -c \
            "UPDATE projects set pool_repository_id = $TARGET_POOL where id = $PROJECT_ID;"
    
    echo starting housekeeping on $PROJECT_ID

    housekeeping $PROJECT_ID

    while :
    do
        sleep 1
        if [ $(get_busy) -le 1 ]
        then
          break
        fi
    done 
   
done
```
